// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  tmdbKey: 'ca571d61521790487fdb62e791d40f66',
  firebase: {
    apiKey: 'AIzaSyDweBZkNvXuoUDjvcI6lxfeCIDsUa2caes',
    authDomain: 'm1-projet-ihm.firebaseapp.com',
    databaseURL: 'https://m1-projet-ihm.firebaseio.com',
    projectId: 'm1-projet-ihm',
    storageBucket: 'm1-projet-ihm.appspot.com',
    messagingSenderId: '128379839868',
    appId: '1:128379839868:web:8e42d61b27390e77d770f9',
    measurementId: 'G-RF9NYP1TW6',
    urlImgDefault: 'gs://m1-projet-ihm.appspot.com/plats/noImg.jpg'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
