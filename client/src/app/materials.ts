import {
  MatButtonModule, MatCheckboxModule, MatExpansionModule, MatGridListModule, MatSidenavModule, MatListModule,
  MatDialogModule,
  MatToolbarModule, MatStepperModule, MatPaginatorModule, MatMenuModule, MatSelectModule, MatInputModule,
  MatBadgeModule,
  MatCardModule, MatChipsModule, MatIconModule, MatTableModule, MatTabsModule, MatProgressSpinnerModule,
  MatAutocompleteModule, MatSnackBarModule
} from '@angular/material';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [MatProgressSpinnerModule, MatButtonModule, MatBadgeModule, MatDialogModule, MatSnackBarModule,
    MatCheckboxModule, MatExpansionModule, MatSidenavModule, MatToolbarModule, MatMenuModule, MatAutocompleteModule,
    MatGridListModule, MatCardModule, MatChipsModule, MatIconModule, MatTableModule, MatTabsModule, MatStepperModule, MatPaginatorModule,
    MatSelectModule, MatInputModule],
  exports: [MatProgressSpinnerModule, MatButtonModule, MatCheckboxModule, MatExpansionModule, MatGridListModule, MatSidenavModule, MatSnackBarModule,
    MatListModule, MatMenuModule, MatSelectModule, MatAutocompleteModule, MatInputModule, MatBadgeModule, MatDialogModule,
    MatCardModule, MatChipsModule, MatIconModule, MatTableModule, MatToolbarModule, MatTabsModule, MatStepperModule, MatPaginatorModule]
})
export class MaterialModule {

}
