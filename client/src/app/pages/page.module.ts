import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../materials';
import {LoaderComponent} from '../partials/loader/loader.component';
import {ImgPlatComponent} from './accueil/img-plat/img-plat.component';
import {AccueilComponent} from './accueil/accueil.component';
import {ListPlatComponent} from './plats/list-plat/list-plat.component';
import {DetailPlatComponent} from './plats/detail-plat/detail-plat.component';
import {CardPlatComponent} from './plats/list-plat/card-plat/card-plat.component';
import {CardFilmComponent} from './films/list-film/card-film/card-film.component';
import {ListFilmComponent} from './films/list-film/list-film.component';
import {DetailFilmComponent} from './films/detail-film/detail-film.component';
import {RouterModule} from '@angular/router';
import { CommandeComponent } from './commande/commande.component';
import {PanierComponent} from './panier/panier.component';
import { DialogSuccessComponent } from './dialog-success/dialog-success.component';
import { PayComponent } from './pay/pay.component';
import { LivraisonComponent } from './livraison/livraison.component';
import { FacturationComponent } from './facturation/facturation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    AccueilComponent,
    ImgPlatComponent,
    DetailPlatComponent,
    ListPlatComponent,
    DetailFilmComponent,
    ListFilmComponent,
    CardFilmComponent,
    CommandeComponent,
    DialogSuccessComponent
  ],
  declarations: [
    PanierComponent,
    LoaderComponent,
    AccueilComponent,
    ImgPlatComponent,
    DetailPlatComponent,
    ListPlatComponent,
    CardPlatComponent,
    DetailFilmComponent,
    ListFilmComponent,
    CardFilmComponent,
    CommandeComponent,
    DialogSuccessComponent,
    PayComponent,
    LivraisonComponent,
    FacturationComponent,
  ],
  exports: [
    CommandeComponent,
    PanierComponent
  ]
})
export class PageModule { }
