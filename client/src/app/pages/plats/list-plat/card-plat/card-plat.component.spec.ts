import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPlatComponent } from './card-plat.component';
import {expect} from "jasmine";

describe('CardPlatComponent', () => {
  let component: CardPlatComponent;
  let fixture: ComponentFixture<CardPlatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardPlatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPlatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
