import {Component, Input, OnInit} from '@angular/core';
import {PlatService} from '../../../../services/plat.service';
import {Environment} from '@angular/compiler-cli/src/ngtsc/typecheck/src/environment';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-card-plat',
  templateUrl: './card-plat.component.html',
  styleUrls: ['./card-plat.component.scss']
})
export class CardPlatComponent implements OnInit {
  @Input() data;
  img$;
  constructor(private servicePlat: PlatService) { }

  async ngOnInit() {
    if (this.data) {
      let urlIMG;
      console.log(this.data.url);
      if (this.data.url === undefined) {
        urlIMG = environment.firebase.urlImgDefault;
      } else {
        urlIMG = this.data.url;
      }
      this.img$ = await this.servicePlat.getImage(urlIMG);
    }

  }


}
