import { Component, OnInit } from '@angular/core';
import {PlatService} from '../../../services/plat.service';
import * as firebase from 'firebase';
import {PageEvent} from '@angular/material';
import {SearchMovieQuery} from '../../../data/searchMovie';

@Component({
  selector: 'app-list-plat',
  templateUrl: './list-plat.component.html',
  styleUrls: ['./list-plat.component.scss']
})
export class ListPlatComponent implements OnInit {
  private plats$;
  private platsTotal$;

  private pageIndexActuel = 1;
  private itemIndexActuel = 1;
  private tabIndexPagesPourPagination = [];
  private nbItemsSurPages = 6;
  private nbPlatsTotal;
  private nbPagesMax: number;

  constructor(private service: PlatService) { }

  async ngOnInit() {
    await (this.service.getNbPlatsTotal()).then((value) => {
      this.nbPlatsTotal = value;
      console.log('nbr de plats total : ' + this.nbPlatsTotal);
    });
    this.nbPagesMax = Math.ceil(this.nbPlatsTotal / this.nbItemsSurPages);

    this.donneMoiDesPlats({pageIndex: this.pageIndexActuel,
                                 pageSize:  this.nbItemsSurPages,
                                 length:    this.nbItemsSurPages});

    console.log(this.plats$);
    console.log('nbr de pages max : ' + this.nbPagesMax);
    console.log('tabIndexPagesPourPagination : ' + this.tabIndexPagesPourPagination);
  }

  /** Met en forme le tableau contenant les pages à afficher. Ex: "1 2 3 4 5 6 ... 500"
  * -> this.nbPagesTotal
  */
  private formerPagination() {
    this.tabIndexPagesPourPagination = [];
    for (let i = this.pageIndexActuel - 2; i <= this.pageIndexActuel + 2 && i <= this.nbPagesMax; i++) {
      if (i > 0) {
        this.tabIndexPagesPourPagination.push(i);
      }
    }
    if (this.tabIndexPagesPourPagination[this.tabIndexPagesPourPagination.length - 1] < this.nbPagesMax) {
      this.tabIndexPagesPourPagination.push('...');
      this.tabIndexPagesPourPagination.push(this.nbPagesMax);
      console.log('Le nbr de pages total est ' + this.nbPagesMax);
    }
  }

  /** Recherche les plats avec filtre 'Tous'
   *  -> this.plats$
   */
  async rechercherPlatsFiltreTous() {
    this.itemIndexActuel = this.pageIndexActuel * this.nbItemsSurPages - this.nbItemsSurPages + 1;
    this.plats$ = this.service.getPlats(this.itemIndexActuel, this.nbItemsSurPages);
  }

  /** Récupère les plats de la page this.pageIndexActuel
   * -> this.plats$
   */
  async donneMoiDesPlats(event: PageEvent) {
    this.pageIndexActuel = event.pageIndex;
    this.formerPagination();

    await this.rechercherPlatsFiltreTous();
  }
}
