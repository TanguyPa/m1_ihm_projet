import { Component, OnInit } from '@angular/core';
import {PlatService} from '../../../services/plat.service';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../../environments/environment';
import {MatDialog} from '@angular/material';
import {DialogSuccess} from '../../../data/DialogSuccess';
import {DialogSuccessComponent} from '../../dialog-success/dialog-success.component';
import {Plat} from '../../../data/Plat';
import {PanierService} from '../../../services/panier.service';

@Component({
  selector: 'app-detail-plat',
  templateUrl: './detail-plat.component.html',
  styleUrls: ['./detail-plat.component.scss']
})
export class DetailPlatComponent implements OnInit {
  private plat$;
  noPlat;
  id;
  img;
  plat;
  platToPanier;
  toConfirm;
  userId;
  constructor(private platService: PlatService,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              private panierServce: PanierService) {
    this.userId = localStorage.getItem('userId');
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.plat$ = this.platService.getPlat(+this.id);
    this.plat$.then(val => {
      this.plat = val;
      this.loadIMG(this.plat.url);
    });
  }
  async loadIMG(url) {
    if (this.plat) {
      let urlIMG;
      console.log(this.plat.url);
      if (this.plat.url === undefined) {
        urlIMG = environment.firebase.urlImgDefault;
      } else {
        urlIMG = this.plat.url;
      }
      await this.platService.getImage(urlIMG).then((img) => {
        this.img = img;
      });
    }
  }

  openDialog(plat: Plat): void {
    this.platToPanier = plat.nomPlat;
    const dialogRef = this.dialog.open(DialogSuccessComponent, {
      width: '600px',
      data: {name: this.platToPanier, url: this.img, confirme: this.toConfirm}
    });
    this.panierServce.ajouterPlatPanier(this.userId, plat.noPlat).then(() => {
      this.panierServce.refreshPanier();
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.toConfirm = result;
      console.log('openDialog:', result);
    });
  }
}
