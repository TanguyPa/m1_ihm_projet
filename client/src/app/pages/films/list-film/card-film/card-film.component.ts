import {Component, Input, OnInit} from '@angular/core';
import {MovieResponse} from '../../../../data/Movie';
import {Routes, RouterModule} from '@angular/router';

@Component({
  selector: 'app-card-film',
  templateUrl: './card-film.component.html',
  styleUrls: ['./card-film.component.scss']
})
export class CardFilmComponent implements OnInit {

  @Input() data: MovieResponse;
  img;

  constructor() {

  }

  ngOnInit() {
    if (this.data.poster_path === null) {
      this.img = '/assets/img/default-placeholder.png';
    } else {
      this.img = 'http://image.tmdb.org/t/p/original/' + this.data.poster_path;
    }

  }


  isTailleDescrOK(overview: string): boolean {
    return overview ? overview.length < 200 : false;
  }

}

