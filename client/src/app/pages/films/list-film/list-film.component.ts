import {Component, OnInit} from '@angular/core';
import {SearchMovieQuery, SearchMovieResponse} from '../../../data/searchMovie';
import {MovieQuery, MovieResponse} from '../../../data/Movie';
import {TmdbService} from '../../../services/tmdb.service';
import {DataTransfertFilmService} from '../../../services/data-transfert-film.service';
import {FilmsService} from '../../../services/films.service';
import {environment} from '../../../../environments/environment';
import {PageEvent} from '@angular/material';
import {Routes, RouterModule, Router} from '@angular/router';
import {KeywordResearchService} from '../../../services/keyword-research.service';
import {SearchService} from '../../../services/search.service';

@Component({
  selector: 'app-list-film',
  templateUrl: './list-film.component.html',
  styleUrls: ['./list-film.component.scss']
})
export class ListFilmComponent implements OnInit {
  private filmsResponses: SearchMovieResponse;
  private multiSearch = false;
  private genres = [];
  private countries = [];
  private queryResearch = '';
  private selectedGenre;
  private pageIndexActuel = 1;
  private nbPagesTotal = [];
  private taillePages = 20;
  private nbFilmsTotal;
  private recherche: SearchMovieQuery = {
    language: 'fr-FR',
    include_adult: undefined,
    year: undefined,
    primary_release_year: undefined,
    region: '',
    query: undefined
  };

  constructor(private searchService: SearchService, private router: Router, private tmdb: TmdbService,
              private dataService: DataTransfertFilmService,
              private keyWordService: KeywordResearchService, private filmsService: FilmsService) {
    this.queryResearch = this.router.getCurrentNavigation()
      .extractedUrl.queryParams ? this.router.getCurrentNavigation().extractedUrl.queryParams.query : undefined;

    if (this.router.getCurrentNavigation().extractedUrl.queryParams && this.router.getCurrentNavigation().extractedUrl.queryParams.genre) {
      this.selectedGenre = this.router.getCurrentNavigation().extractedUrl.queryParams.genre;
      this.recherche.with_genres = this.selectedGenre;
    }
    searchService.setSearcCallBack((query) => {
      if (query === '') {
        this.init();
        return;
      }
      this.keyWordService.multiSearch(query).subscribe((r) => {
        this.filmsResponses = r;
        this.nbFilmsTotal = this.filmsResponses ? this.filmsResponses.total_results : 0;

        this.taillePages = this.filmsResponses ? this.filmsResponses.results.length : 0;

        this.formerPagination();
      });
    });

    searchService.setResetGenre(() => {
      this.selectedGenre = undefined;
    });
  }

  ngOnInit(): void {
    this.init();
  }

  async init() {
    this.getFilmsGenres();
    this.tmdb.init(environment.tmdbKey);
    if (this.queryResearch) {
      await this.keyWordService.multiSearch(this.queryResearch).subscribe((r) => {
        this.filmsResponses = r;
        this.nbFilmsTotal = this.filmsResponses ? this.filmsResponses.total_results : 0;
        this.taillePages = this.filmsResponses ? this.filmsResponses.results.length : 0;
        this.formerPagination();
      });
    } else {
      if (!this.multiSearch) {
        await this.rechercherFilmsPopulaires();
      } else {
      }
    }


  }

  /** Met en forme le tableau contenant les pages à afficher. Ex: "1 2 3 4 5 6 ... 500"
   * -> this.nbPagesTotal
   */
  private formerPagination() {
    this.nbPagesTotal = [];
    for (let i = this.pageIndexActuel - 2; i <= this.pageIndexActuel + 2 && i <= this.filmsResponses.total_pages; i++) {
      if (i > 0) {
        this.nbPagesTotal.push(i);
      }
    }
    if (this.nbPagesTotal[this.nbPagesTotal.length - 1] < this.filmsResponses.total_pages) {
      this.nbPagesTotal.push('...');
      this.nbPagesTotal.push(this.filmsResponses.total_pages);
    }
  }

  /** Recherche les films populaires
   *  -> this.filmsResponses
   */
  async rechercherFilmsPopulaires(pageIndex?: number) {
    this.recherche.page = pageIndex;

    if (this.queryResearch) {
      await this.keyWordService.multiSearch(this.queryResearch, 'fr', pageIndex).subscribe((r) => {
        this.filmsResponses = r;
      });
    } else {
      await this.tmdb.discoverFilms(this.recherche).then((value) => {
        this.filmsResponses = value;
      });
    }
  }

  async donneMoiDesFilms(event: PageEvent) {
    this.pageIndexActuel = event.pageIndex;
    await this.loadFilms();
  }

  async getFilmsGenres() {
    await this.filmsService.getFilmsGenres().subscribe((genre => {
      this.genres = genre.genres;
    }));
  }

  async onGenreChange(id) {
    this.searchService.resetSearch();
    this.queryResearch = '';
    if (id === undefined) {
      this.resetRecherche();
      this.recherche.sort_by = 'popularity.desc';
      this.pageIndexActuel = 1;
      await this.loadFilms();
    } else {
      this.router.navigate(['/films'], {queryParams: {genre: id}});
      this.recherche.sort_by = 'popularity.desc';
      this.recherche.with_genres = `${id}`;
      this.pageIndexActuel = 1;
      await this.loadFilms();
    }

  }
  async loadFilms() {
    this.formerPagination();
    await this.rechercherFilmsPopulaires(this.pageIndexActuel);
  }

  resetRecherche() {
    this.recherche = {
      language: 'fr-FR',
      include_adult: undefined,
      year: undefined,
      primary_release_year: undefined,
      region: '',
      query: undefined
    };
  }
}
