import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FilmsService} from '../../../services/films.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MovieResponse} from '../../../data/Movie';
import {MatTabsModule} from '@angular/material/tabs';
import { Routes, RouterModule } from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {PanierService} from '../../../services/panier.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {DialogSuccessComponent} from '../../dialog-success/dialog-success.component';

@Component({
  selector: 'app-detail-film',
  templateUrl: './detail-film.component.html',
  styleUrls: ['./detail-film.component.scss']
})
export class DetailFilmComponent implements OnInit, OnChanges {
  id;
  movie$;
  credits$;
  reviews$;
  reviews;
  credits;
  videos$;
  videos;
  filmsSimilaires$;
  filmsSimilaires;
  movieReponse: MovieResponse;
  userId;
  toConfirm;
  constructor(private findFilmService: FilmsService, private panierService: PanierService, private snackBar: MatSnackBar,
              private router: Router, private route: ActivatedRoute, private _sanitizer: DomSanitizer, public dialog: MatDialog) {
    this.userId =  localStorage.getItem('userId');
  }

  ngOnInit() {
    this.loadFilm();
    this.router.events.subscribe(() => {
      this.loadFilm();
    });
  }
  ngOnChanges (changes: SimpleChanges) {
    console.log('changes', changes);
  }
  loadFilm() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.movie$ = this.findFilmService.getFilmByID(this.id);
    this.credits$ = this.findFilmService.getFilmCreditsById(this.id);
    this.reviews$ = this.findFilmService.getReviews(this.id);
    this.videos$ = this.findFilmService.getVideos(this.id);
    this.filmsSimilaires$ = this.findFilmService.getFilmsSimilaires(this.id);
    this.credits$.subscribe(val => {
      this.credits = val['cast'];
      console.log('cast:', this.credits);
    });
    this.movie$.subscribe(val => {
      this.movieReponse = val;
      console.log(this.movieReponse);
    });
    this.reviews$.subscribe(val => {
      this.reviews = val['results'];
    });
    this.videos$.subscribe(val => {
      this.videos = val['results'];
    });
    this.filmsSimilaires$.subscribe(val => {
      this.filmsSimilaires = val['results'];
    });
  }
  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
  }
  transformeVideo(url) {
    return this._sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  imageCreditDefault(value) {
    if (value) {
      return 'https://image.tmdb.org/t/p/w200/' + value;
    } else {
      return '/assets/img/imgDefault.png';
    }
  }
  ajouterFilmPanier(movie) {
    this.panierService.getPanier(this.userId).then((panier) => {
      console.log('PANIER', panier.films);
      if (!panier.films.includes(this.id)) {
        this.panierService.ajouterFilmPanier(this.userId, this.id).then(() => {
          this.panierService.refreshPanier();
        });

        const dialogRef = this.dialog.open(DialogSuccessComponent, {
          width: '600px',
          data: {
            name: movie.original_title,
            url: movie.poster_path ? 'https://image.tmdb.org/t/p/original/' + movie.poster_path : '/assets/img/default-placeholder.png',
            confirme: this.toConfirm}
        });

        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          this.toConfirm = result;
          console.log('openDialog:', result);
        });
      } else {
        this.snackBar.open('Film déjà présent dans le panier', 'Ok', {
          duration: 2000,
        });
      }
    });
  }
}
