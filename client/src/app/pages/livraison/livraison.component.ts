import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Adresse} from '../../data/Adresse';
import {FormControl, FormControlName} from '@angular/forms';
import {AdresseService} from '../../services/adresse.service';
import {CommandeService} from '../../services/commande.service';
import {PanierService} from '../../services/panier.service';
import {MatStepper} from '@angular/material';
@Component({
  selector: 'app-livraison',
  templateUrl: './livraison.component.html',
  styleUrls: ['./livraison.component.scss']
})
export class LivraisonComponent implements OnInit {
  @Output() onComplete = new EventEmitter();
  adresse: Adresse;
  nomRueControl = new FormControl();
  myControlGroup = new FormControl();
  filteredOptions;
  query;
  uid;
  constructor(private serviceAdresse: AdresseService, private commandeService: CommandeService, private panierService: PanierService) {
    this.uid =  localStorage.getItem('userId');
    this.adresse = {
      nRue: null,
      name: '',
      city: '',
      postcode: ''
    };
  }

  ngOnInit() {
    this.nomRueControl.valueChanges
      .subscribe({
        next: (value) => {
          if (!value) {
            return;
          }
          console.log('value', value);
          this.serviceAdresse.searchRue(value).subscribe((r) => {
            console.log('r', r);
            this.filteredOptions = r.features;
          });
        }
      });
  }
  onChangeNomRue(object) {
    if (object) {
      this.adresse.name = object['properties'].name;
      this.adresse.city = object['properties'].city;
      this.adresse.postcode = object['properties'].postcode;
    }
  }
  adresseToString(a: Adresse): String {
    return a.nRue + ' ' + a.name + ' ' + a.city + ' ' + a.postcode;
  }
  async saveCommande() {
    if (this.adresse.postcode === '' || this.adresse.nRue === null || this.adresse.name === '' || this.adresse.city === '') {
      return;
    }
    console.log('adresse de commande', this.adresse);
    console.log('adresse de commande', this.adresseToString(this.adresse));
    await this.commandeService.enregistrerCommande(this.uid, this.adresse);
    this.panierService.refreshPanier();
    this.onComplete.emit();
  }
}
