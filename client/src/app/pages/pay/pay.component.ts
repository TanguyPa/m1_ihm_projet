import {Component, OnInit, ViewChild} from '@angular/core';
import {MatStepper} from '@angular/material';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.scss']
})
export class PayComponent implements OnInit {
  @ViewChild('stepper', {static: false}) private myStepper: MatStepper;
  completePanier = false;
  completeLivraison = false;
  constructor() { }

  ngOnInit() {
  }

  onCompletePanier() {
    this.myStepper.selected.completed = true;
    this.completePanier = true;
  }

  onCompleteLivraison() {
    this.myStepper.selected.completed = true;
    this.completeLivraison = true;
    this.myStepper.next();
  }
}
