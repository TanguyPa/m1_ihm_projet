import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgPlatComponent } from './img-plat.component';

describe('ImgPlatComponent', () => {
  let component: ImgPlatComponent;
  let fixture: ComponentFixture<ImgPlatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgPlatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgPlatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
