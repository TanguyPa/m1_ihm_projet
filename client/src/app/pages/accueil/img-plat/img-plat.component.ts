import {Component, Input, OnInit} from '@angular/core';
import {PlatService} from '../../../services/plat.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-img-plat',
  templateUrl: './img-plat.component.html',
  styleUrls: ['./img-plat.component.scss']
})
export class ImgPlatComponent implements OnInit {
  @Input() data;
  img$;
  constructor(private servicePlat: PlatService) { }

  async ngOnInit() {
    if (this.data) {
      let urlIMG;
      console.log(this.data.url);
      if (this.data.url === undefined) {
        urlIMG = environment.firebase.urlImgDefault;
      } else {
        urlIMG = this.data.url;
      }
      this.img$ = await this.servicePlat.getImage(urlIMG);
    }
  }

}
