import {Component, OnInit, ViewChild} from '@angular/core';
import {FilmsService} from '../../services/films.service';
import {PlatService} from '../../services/plat.service';
import {Routes, RouterModule} from '@angular/router';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  filmIMG1 = [];
  filmIMG2 = [];
  platIMG1 = [];
  platIMG2 = [];
  films$;
  plats$;
  isLoading = true;
  constructor(private serviceFilm: FilmsService, private servicePlat: PlatService) {}

  ngOnInit() {
    this.films$ = this.serviceFilm.getPopularMovie();
    this.plats$ = this.servicePlat.getPlats(0, 10);
    this.films$.subscribe((films) => {
      this.filmIMG1.push(films.results[0]);
      this.filmIMG1.push(films.results[1]);
      this.filmIMG1.push(films.results[2]);
      this.filmIMG1.push(films.results[3]);
      this.filmIMG2.push(films.results[4]);
      this.filmIMG2.push(films.results[5]);
      this.filmIMG2.push(films.results[6]);
      this.filmIMG2.push(films.results[7]);
      // console.log(this.filmIMG1);
      // console.log(this.filmIMG2);
    });
    this.plats$.then((plats) => {
      console.log('plats', plats);
      this.platIMG1.push(plats[0]);
      this.platIMG1.push(plats[1]);
      this.platIMG1.push(plats[2]);
      this.platIMG1.push(plats[3]);
      this.platIMG2.push(plats[4]);
      this.platIMG2.push(plats[5]);
      this.platIMG2.push(plats[6]);
      this.platIMG2.push(plats[7]);
    });
    this.isLoading = false;
  }
}
