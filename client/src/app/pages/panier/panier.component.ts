import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Plat} from '../../data/Plat';
import {MovieResponse} from '../../data/Movie';
import {PanierService} from '../../services/panier.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {PlatService} from '../../services/plat.service';
import {FilmsService} from '../../services/films.service';
import {CommandeService} from '../../services/commande.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {
  @Output() onComplete = new EventEmitter();
  private panier;
  private plats: Plat[] = [];
  private films = [];

  private uid: string;
  constructor(private panierService: PanierService,
              private afAuth: AngularFireAuth,
              private platService: PlatService,
              private filmService: FilmsService,
              private commandeService: CommandeService
  ) {}

  async ngOnInit() {
    this.uid =  localStorage.getItem('userId');
    await this.getPanier();
  }

  async getPanier() {
    // Attente de la promesse pour récupérer le panier
    await this.panierService.getPanier(this.uid).then(async value => {
      this.panier = value;
      // Attente de la promesse pour récupérer les différents plats
      const countsPlat = {};
      this.panier['plats'].forEach(function(x) {
        countsPlat[x] = (countsPlat[x] || 0) + 1;
      });
      this.panier['plats'] = [...new Set(this.panier['plats'])];
      const platsFetch = [];
      for (const idPlat of this.panier['plats']) {
        await this.platService.getPlat(+idPlat).then((plat) => {
          plat.quantite = countsPlat[idPlat];
          platsFetch.push(plat);
        });
      }
      this.plats = platsFetch;
      this.loadIMG();
      const filmsFetch = [];
      for (const idFilm of this.panier['films']) {
        await this.filmService.getFilmByID(idFilm).subscribe((films) => {
          filmsFetch.push(films);
        });
      }
      if (this.plats.length > 0 || this.panier['films'].length > 0) {
        this.onComplete.emit();
      }
      this.films = filmsFetch;
    });
  }

  async loadIMG() {
    if (this.plats) {
      for (const p of this.plats) {
        let urlIMG;
        if (p.url === undefined) {
          urlIMG = environment.firebase.urlImgDefault;
        } else {
          urlIMG = p.url;
        }
        await this.platService.getImage(urlIMG).then((img) => {
          p.img = img;
        });
      }
    }
  }

  getFilmImg(poster) {
    if (poster === null) {
      return '/assets/img/default-placeholder.png';
    } else {
      return 'http://image.tmdb.org/t/p/original/' + poster;
    }
  }

  getTotal() {
    let montant = this.films.length * 6;
    this.plats.forEach((p) => {
      montant += p.quantite ? p.prix * p.quantite : p.prix;
    });
    return montant;
  }

  async ajouterPlatPanier(noPlat: number) {
    await this.panierService.ajouterPlatPanier(this.uid, noPlat);
    for (const p of this.plats) {
      if (p.noPlat === noPlat) {
        p.quantite++;
      }
    }
    this.panierService.refreshPanier();
  }

  async suppPlatPanier(noPlat: number) {
    await this.panierService.supprimerPlatPanier(this.uid, noPlat);
    let indexToPop;
    for (const p of this.plats) {
      if (p.noPlat === noPlat) {
        if (p.quantite === 1) {
          indexToPop = this.plats.indexOf(p);
        } else {
          p.quantite--;
        }
        break;
      }
    }
    if (indexToPop) {
      this.getPanier();
    }
    this.panierService.refreshPanier();
  }

  async suppAllPlatPanier(noPlat: number) {
    let platToDelete;
    this.plats.forEach((p, i) => {
      if (p.noPlat === noPlat) {
        platToDelete = { plat: p, index: i};
      }
    });
    if (!platToDelete) {
      return;
    }
    for (let i = 0; i < platToDelete.plat.quantite; i++) {
      await this.panierService.supprimerPlatPanier(this.uid, platToDelete.plat.noPlat);
    }
    this.plats.splice(platToDelete.plat.index, 1);
    this.panierService.refreshPanier();
  }

  async suppFilmPanier(noFilm: number) {
    await this.panierService.supprimerFilmPanier(this.uid, noFilm);
    this.getPanier();
    this.panierService.refreshPanier();
  }


  async ajouterFilmPanier(noFilm: number) {
    await this.panierService.ajouterFilmPanier(this.uid, noFilm);
    this.getPanier();
    this.panierService.refreshPanier();
  }

}
