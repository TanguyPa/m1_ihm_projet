import { Component, OnInit } from '@angular/core';
import {CommandeService} from '../../services/commande.service';
import {FilmsService} from '../../services/films.service';
import {PlatService} from '../../services/plat.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.scss']
})
export class CommandeComponent implements OnInit {
  commandes = [];
  uid;
  constructor(private commandeService: CommandeService, private filmService: FilmsService, private platService: PlatService) {
    this.uid =  localStorage.getItem('userId');
  }

  ngOnInit() {
    this.commandeService.getCommande(this.uid).then(commandes => {
      this.commandes = commandes;
      if (this.commandes.length > 0) {
        this.openSeeMore(this.commandes[0]);
      }
/*      commandes.sort((a, b) => {

      });*/
    });
  }

  getTime(dateStr) {
    const date = new Date(dateStr);
    const options = {
      year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric'
    };

    return date.toLocaleDateString('fr', options);
  }

  buildAdresseString(adresse) {
    let adresseString = '';
    if (adresse.nRue) {
      adresseString += adresse.nRue + ' ';
    }
    if (adresse.name) {
      adresseString += adresse.name + ' ';
    }
    if (adresse.city) {
      adresseString += adresse.city + ' ';
    }
    if (adresse.postcode) {
      adresseString += adresse.postcode;
    }
    return adresseString;
  }

  async openSeeMore(commande) {
    commande.openSeeMore = !commande.openSeeMore;
    if (commande.filmsData && commande.platsData) {return;}
    const filmsData = [];
    await commande.films.forEach(async f => {
      await this.filmService.getFilmByID(f).subscribe(film => {
        filmsData.push(film);
      });
    });
    const countsPlat = {};


    commande.plats.forEach(function(x) {
      countsPlat[x] = (countsPlat[x] || 0) + 1;
    });
    commande.plats = [...new Set(commande.plats)];
    const platsData = [];
    await commande.plats.forEach(async p => {
      await this.platService.getPlat(p).then(async plat => {
        let urlIMG;
        if (plat.url === undefined) {
          urlIMG = environment.firebase.urlImgDefault;
        } else {
          urlIMG = plat.url;
        }
        await this.platService.getImage(urlIMG).then((img) => {
          plat.img = img;
        });
        plat.quantite = countsPlat[p];
        platsData.push(plat);
      });
    });
    commande.filmsData = filmsData;
    commande.platsData = platsData;
  }

  getFilmImg(poster) {
    if (poster === null) {
      return '/assets/img/default-placeholder.png';
    } else {
      return 'http://image.tmdb.org/t/p/original/' + poster;
    }
  }

}
