import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import DocumentData = firebase.firestore.DocumentData;
import {DataService} from './data.service';
import {User} from 'firebase';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  private refreshPanierCallback = () => {};

  constructor(private dataService: DataService) {
  }

  setRefreshPanierCallback (refreshPanierCallback) {
    this.refreshPanierCallback = refreshPanierCallback;
  }

  refreshPanier () {
    this.refreshPanierCallback();
  }

  async getPanier(uid: string): Promise<any> {
    let panier: any;
    await this.dataService.getFireStore().collection('Panier').where('userId', '==', uid).get().then((querySnapshot) => {
      const panierData: DocumentData = querySnapshot.docs[0];
      panier = {
        userId: uid,
        documentPath: panierData.ref.path,
        plats: panierData.data()['plats'],
        films: panierData.data()['films']
      };
    }).catch(reason => {
      console.log('Erreur lors de la récupération du panier' + reason);
    });
    return panier;
  }

  async resetPanier(uid: string) {
    await this.dataService.getFireStore().collection('Panier').where('userId', '==', uid).get().then(async (querySnapshot) => {
      if (!querySnapshot.empty) {
        await this.dataService.getFireStore().doc(querySnapshot.docs[0].ref.path).update({
          plats: [],
          films: []
        });
      }
    });
  }

  async setPanier(panier) {
    await this.dataService.getFireStore().doc(panier['documentPath']).update({
      plats: panier['plats'],
      films: panier['films']
    });
  }

  async initPanier(uid: string) {
    await this.dataService.getFireStore().collection('Panier').where('userId', '==', uid).get().then(async (querySnapshot) => {
      if (querySnapshot.empty) {
        await this.dataService.getFireStore().collection('Panier').add({
          userId: uid,
          plats: [],
          films: []
        });
      }
    }).catch(reason => {
      console.log('Erreur lors de l initialisation du panier' + reason);
    });
  }

  async ajouterPlatPanier(uid: string, idPlat: number) {
    await this.getPanier(uid).then(async value => {
      const panier = value;
      const plats: number[] = panier['plats'];
      plats.push(idPlat);
      panier['plats'] = plats;
      await this.setPanier(panier);
    });
  }

  async ajouterFilmPanier(uid: string, idFilm: number) {
    await this.getPanier(uid).then(async value => {
      const panier = value;
      const films: number[] = panier['films'];
      films.push(idFilm);
      panier['films'] = films;
      await this.setPanier(panier);
    });
  }

  async supprimerPlatPanier(uid: string, idPlat: number) {
    await this.getPanier(uid).then(async value => {
      const panier = value;
      const plats: number[] = panier['plats'];
      plats.splice(plats.indexOf(idPlat), 1);
      panier['plats'] = plats;
      await this.setPanier(panier);
    });
  }

  async supprimerFilmPanier(uid: string, idFilm: number) {
    await this.getPanier(uid).then(async value => {
      const panier = value;
      const films: number[] = panier['films'];
      films.splice(films.indexOf(idFilm), 1);
      panier['films'] = films;
      await this.setPanier(panier);
    });
  }
}
