import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/index';
import {HttpClient, HttpParams} from '@angular/common/http';
const API = 'https://api.themoviedb.org/3';
const API_KEY = environment.tmdbKey;
@Injectable({
  providedIn: 'root'
})
export class KeywordResearchService {

  constructor(private http: HttpClient) { }
  multiSearch(query, language?, page?, include_adult?, region?): Observable<any> {
    const url = `${API}/search/movie`;
    let params = new HttpParams();
    if (language) {
      params = params.append('language', language);
    }
    if (page) {
      params = params.append('page', page);
    }
    if (include_adult) {
      params = params.append('include_adult', include_adult);
    }
    if (region) {
      params = params.append('region', region);
    }
    params = params.append('api_key', environment.tmdbKey);
    params = params.append('query', query);
    return this.http.get(url, {params: params});
  }
}
