import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class SearchService {
  private searchCallBack = (query) => {};
  private resetSearchCallBack = () => {};
  private resetGenreCallBack = () => {};

  constructor () { }

  setSearcCallBack (searchCallBack) {
    this.searchCallBack = searchCallBack;
  }

  search (query) {
    this.searchCallBack(query);
  }

  setResetSearch (resetSearchCallBack) {
    this.resetSearchCallBack = resetSearchCallBack;
  }

  resetSearch () {
    this.resetSearchCallBack();
  }

  setResetGenre (resetGenreCallBack) {
    this.resetGenreCallBack = resetGenreCallBack;
  }

  resetGenre () {
    this.resetGenreCallBack();
  }
}
