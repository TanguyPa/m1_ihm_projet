import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {TmdbService} from './tmdb.service';

@Injectable({
  providedIn: 'root'
})
export class DataTransfertFilmService {
  listeFilms: number[] = [];
  idClient: string;
  complete: boolean;
  precedent: BehaviorSubject<string>;
  filtreDateCommande = '0';


  constructor(private http: HttpClient, private tmbd: TmdbService) {
    this.complete = false;
    this.precedent = new BehaviorSubject<string>('non');
  }


  ajouterFilm(id: number) {
    this.listeFilms.push(id);
  }

  enleverFilm(id: number) {
    this.listeFilms = this.listeFilms.filter((value => value !== id));
  }

  statutFilm(id: number): boolean {
    let i = 0;
    let estChoisi = false;
    while (i < this.listeFilms.length  && this.listeFilms[i] !== id) {
      i++;
    }
    if (this.listeFilms[i] === id) {
      estChoisi = true;
    }

    return estChoisi;
  }

  listeFilmsCommandes(): string {
    let i = 0;
    let liste = '';
    while (i < this.listeFilms.length) {
      liste = liste + this.listeFilms[i].toString() + ';';
      i++;
    }
    liste = liste.substring(0, liste.length - 1);
    return liste;
  }


  POST(url, params: {[key: string]: string}): Promise<HttpResponse<string>> {
    const P = new HttpParams( {fromObject: params} );
    console.log(params, P, P.toString());
    return this.http.post( url, P, {
      observe: 'response',
      responseType: 'text',
      headers: {'content-type': 'application/x-www-form-urlencoded'}
    }).toPromise();
  }

  GET(url, params: {[key: string]: string}): Promise<HttpResponse<string>> {
    const P = new HttpParams( {fromObject: params} );
    return this.http.get( url, {
      observe: 'response',
      responseType: 'text',
      headers: {'content-type': 'application/x-www-form-urlencoded'}
    }).toPromise();
  }

}
