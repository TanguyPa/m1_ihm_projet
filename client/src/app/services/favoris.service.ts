import { Injectable } from '@angular/core';
import {DataService} from './data.service';
import {DocumentData} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FavorisService {

  private refreshFavorisCallback = () => {};

  constructor(private dataService: DataService) {
  }

  setRefreshFavorisCallback (refreshFavorisCallback) {
    this.refreshFavorisCallback = refreshFavorisCallback;
  }

  refreshFavoris () {
    this.refreshFavorisCallback();
  }

  async setFavoris(favoris) {
    await this.dataService.getFireStore().doc(favoris['documentPath']).update({
      films: favoris['films']
    });
  }

  async getFavoris(uid: string): Promise<any> {
    let favoris: any;
    await this.dataService.getFireStore().collection('Favoris').where('userId', '==', uid).get().then((querySnapshot) => {
      const favorisData: DocumentData = querySnapshot.docs[0];
      favoris = {
        userId: uid,
        documentPath: favorisData.ref.path,
        films: favorisData.data()['films']
      };
    }).catch(reason => {
      console.log('Erreur lors de la récupération des favoris' + reason);
    });
    return favoris;
  }

  async initFavoris(uid: string) {
    await this.dataService.getFireStore().collection('Favoris').where('userId', '==', uid).get().then(async (querySnapshot) => {
      if (querySnapshot.empty) {
        await this.dataService.getFireStore().collection('Favoris').add({
          userId: uid,
          films: []
        });
      }
    }).catch(reason => {
      console.log('Erreur lors de l initialisation des favoris' + reason);
    });
  }

/*  async ajouterPlatFavoris(uid: string, idPlat: number) {
    await this.getFavoris(uid).then(async value => {
      const favoris = value;
      const plats: number[] = favoris['plats'];
      plats.push(idPlat);
      favoris['plats'] = plats;
      await this.setFavoris(favoris);
    });
  }*/

  async ajouterFilmFavoris(uid: string, idFilm: number) {
    await this.getFavoris(uid).then(async value => {
      const favoris = value;
      const films: number[] = favoris['films'];
      films.push(idFilm);
      favoris['films'] = films;
      await this.setFavoris(favoris);
    });
  }

/*  async supprimerPlatFavoris(uid: string, idPlat: number) {
    await this.getFavoris(uid).then(async value => {
      const favoris = value;
      const plats: number[] = favoris['plats'];
      plats.splice(plats.indexOf(idPlat), 1);
      favoris['plats'] = plats;
      await this.setFavoris(favoris);
    });
  }*/
  async supprimerFilmFavoris(uid: string, idFilm: number) {
    await this.getFavoris(uid).then(async value => {
      const favoris = value;
      const films: number[] = favoris['films'];
      films.splice(films.indexOf(idFilm), 1);
      favoris['films'] = films;
      await this.setFavoris(favoris);
    });
  }
}
