import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import * as firebase from 'firebase';
import {FirebaseFirestore, FirebaseStorage} from '@angular/fire';
import {Plat} from '../data/Plat';

@Injectable({
  providedIn: 'root'
})


export class PlatService {
  private firestore: FirebaseFirestore;
  private storage: FirebaseStorage;
  constructor() {
    // Initialize Cloud Firestore through Firebase
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: environment.firebase.apiKey,
        authDomain: environment.firebase.authDomain,
        projectId:  environment.firebase.projectId
      });
    }
    this.firestore = firebase.firestore();
  }

  async getImage(url): Promise<string> {
    return await firebase.storage().refFromURL(url).getDownloadURL();
  }

   async getPlat(index: number): Promise<Plat> {
      let plat: Plat;
      await this.firestore.collection('Plat').where('noPlat', '==', index).get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          plat = {
            noPlat: doc.data()['noPlat'],
            nomPlat: doc.data()['nomPlat'],
            prix: doc.data()['prix'],
            ingredients: doc.data()['ingredients'],
            url: doc.data()['url']
          };
        });
      }).catch((error) => {
        console.log('Erreur lors de la récupération du plat ' + error);
      });
      return plat;
  }

  async getPlats(index: number, nbPlats: number): Promise<Plat[]> {
    const plats: Plat[] = [];

    await this.firestore.collection('Plat').where('noPlat',
      '>=', index).where('noPlat', '<', index + nbPlats)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          const plat: Plat = {
            noPlat: doc.data()['noPlat'],
            nomPlat: doc.data()['nomPlat'], prix: doc.data()['prix'], ingredients: doc.data()['ingredients'], url: doc.data()['url']
          };
          plats.push(plat);

        });

      }).catch((error) => {
        console.log('Erreur lors de la récupération du plat ' + error);
      });
    return plats;
  }


  async getNbPlatsTotal(): Promise<number> {
    const plats: Plat[] = [];

    await this.firestore.collection('Plat').where('noPlat',
      '>=', 1)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          const plat: { noPlat: any } = {
            noPlat: doc.data()['noPlat']
          };
          plats.push(<Plat>plat);
        });

      }).catch((error) => {
        console.log('Erreur lors de la récupération du plat ' + error);
      });
    return plats.length;
    }

}
