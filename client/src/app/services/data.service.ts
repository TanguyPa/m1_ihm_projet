import { Injectable } from '@angular/core';
import {auth, User} from 'firebase';
import {environment} from '../../environments/environment';
import {FirebaseAuth, FirebaseFirestore} from '@angular/fire';
import * as firebase from 'firebase';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  private fireStore: FirebaseFirestore;

  constructor(private afAuth: AngularFireAuth) {
    // Initialize Cloud Firestore through Firebase
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: environment.firebase.apiKey,
        authDomain: environment.firebase.authDomain,
        projectId:  environment.firebase.projectId
      });
    }
    this.fireStore = firebase.firestore();
  }

  getFireStore(): FirebaseFirestore {
    return this.fireStore;
  }
}
