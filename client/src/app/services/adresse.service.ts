import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/index';
const API = 'https://api-adresse.data.gouv.fr/search/';

@Injectable({
  providedIn: 'root'
})
export class AdresseService {

  constructor(private http: HttpClient) { }
  searchRue(nomRue): Observable<any> {
    let params = new HttpParams();
    if (nomRue) {
      params = params.append('q', nomRue);
    }
    return this.http.get<any>(API, {params: params});
  }
}
