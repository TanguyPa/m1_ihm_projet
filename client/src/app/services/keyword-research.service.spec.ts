import { TestBed } from '@angular/core/testing';

import { KeywordResearchService } from './keyword-research.service';

describe('KeywordResearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KeywordResearchService = TestBed.get(KeywordResearchService);
    expect(service).toBeTruthy();
  });
});
