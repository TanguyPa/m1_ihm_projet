import { Injectable } from '@angular/core';
import {DataService} from './data.service';
import {PanierService} from './panier.service';
import {PlatService} from './plat.service';
import DocumentData = firebase.firestore.DocumentData;

@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  constructor(private dataService: DataService, private panierService: PanierService, private platService: PlatService) { }

  async enregistrerCommande(userid: string, adresseLiv: object) {
    await this.panierService.getPanier(userid).then(async (value) => {
      await this.dataService.getFireStore().collection('Commande').add({
        userId: value['userId'],
        plats: value['plats'],
        films: value['films'],
        dateCommande: Date.now(),
        adresseLivraison: adresseLiv,
        montant: await this.getMontantCommande(value['films'], value['plats'])
      }).then(async (value1) => {
        await this.panierService.resetPanier(userid).catch((reason) => {
          console.log('Erreur lors du reset du panier' + reason);
        });
      }).catch(reason => {
        console.log('Erreur lors de l enregistrement de la commande' + reason);
      });
    });
  }

  async getCommande(uid: string): Promise<any> {
    const panier = [];
    await this.dataService.getFireStore().collection('Commande')
      .where('userId', '==', uid).orderBy('dateCommande', 'desc').get().then((querySnapshot) => {
      for (let i = 0; i < querySnapshot.docs.length; i++) {
        const commandeData: DocumentData = querySnapshot.docs[i];
        panier.push(commandeData.data());
      }
    }).catch(reason => {
      console.log('Erreur lors de la récupération du panier' + reason);
    });
    console.log('return commande', panier);
    return panier;
  }

  async getMontantCommande(films: any[], plats: any[]): Promise<number> {
    let montant = 0;
    for (const value of films) {
      montant += 6;
    }
    for (const value of plats) {
      await this.platService.getPlat(value).then((plat) => {
        montant += plat['prix'];
      });
    }
    return montant;
  }
}
