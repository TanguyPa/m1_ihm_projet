import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {MovieGenre, MovieResponse} from '../data/Movie';
import {Observable} from 'rxjs/index';

const API = 'https://api.themoviedb.org/3';
const API_KEY = environment.tmdbKey;

@Injectable({
  providedIn: 'root'
})


export class FilmsService {
  constructor(private http: HttpClient) {
  }
  getPopularMovie(page?): Observable<any> {
    const url = `${API}/movie/popular`;
    let params = new HttpParams();
    params = params.append('api_key', API_KEY);
    if (page) {
      params = params.append('page', page);
    }
    return this.http.get<any>(url, {params: params});
  }

  getFilmByID(id): Observable<MovieResponse> {
    const url = `${API}/movie/` + id;
    let params = new HttpParams();
    params = params.append('api_key', API_KEY);
    params = params.append('language', 'fr-FR');
    return this.http.get<MovieResponse>(url, {params: params});
  }

  getFilmCreditsById(id): Observable<any> {
    const url = `${API}/movie/` + id + '/credits';
    let params = new HttpParams();
    params = params.append('api_key', API_KEY);
    params = params.append('language', 'fr-FR');
    return this.http.get<any>(url, {params: params});
  }
  getReviews(id): Observable<any> {
    const url = `${API}/movie/` + id + '/reviews';
    let params = new HttpParams();
    params = params.append('api_key', API_KEY);
    params = params.append('language', 'fr-FR');
    return this.http.get<any>(url, {params: params});
  }
  getVideos(id): Observable<any> {
    const url = `${API}/movie/` + id + '/videos';
    let params = new HttpParams();
    params = params.append('api_key', API_KEY);
    params = params.append('language', 'fr-FR');
    return this.http.get<any>(url, {params: params});
  }
  getFilmsSimilaires(id): Observable<any> {
    const url = `${API}/movie/` + id + '/similar';
    let params = new HttpParams();
    params = params.append('api_key', API_KEY);
    params = params.append('language', 'fr-FR');
    return this.http.get<any>(url, {params: params});
  }
  getCountries() {
    const url = `${API}/configuration/languages`;
    let params = new HttpParams();
    params = params.append('api_key', API_KEY);
    return this.http.get<any>(url, {params: params});
  }
  getFilmsGenres(): Observable<any> {
    const url = `${API}/genre/movie/list`;
    let params = new HttpParams();
    params = params.append('api_key', API_KEY);
    params = params.append('language', 'fr-FR');
    return this.http.get<any>(url, {params: params});
  }
}
