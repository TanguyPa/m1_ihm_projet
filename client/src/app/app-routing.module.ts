import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccueilComponent} from './pages/accueil/accueil.component';
import {ListFilmComponent} from './pages/films/list-film/list-film.component';
import {DetailFilmComponent} from './pages/films/detail-film/detail-film.component';
import {DetailPlatComponent} from './pages/plats/detail-plat/detail-plat.component';
import {ListPlatComponent} from './pages/plats/list-plat/list-plat.component';
import {PayComponent} from './pages/pay/pay.component';
import {CommandeComponent} from './pages/commande/commande.component';

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'films', component: ListFilmComponent},
  { path: 'films/:id', component: DetailFilmComponent},
  { path: 'plats', component: ListPlatComponent},
  { path: 'plats/:id', component: DetailPlatComponent},
  { path: 'commandes', component: CommandeComponent},
  { path: 'panier', component: PayComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
