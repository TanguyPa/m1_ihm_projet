export interface Adresse {
  nRue: number;
  name: string;
  city: string;
  postcode: string;
}
