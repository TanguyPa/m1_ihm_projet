export interface Plat {
  noPlat: number;
  nomPlat: string;
  prix: number;
  ingredients: string[];
  url: string;
  quantite?: number;
  img?: string;
}
