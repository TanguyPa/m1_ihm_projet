export interface DialogSuccess {
  name: string;
  url: string;
  confirme: boolean;
}
