import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {auth, User} from 'firebase';
import {FormControl} from '@angular/forms';
import {KeywordResearchService} from '../../services/keyword-research.service';
import {PanierService} from '../../services/panier.service';
import {Plat} from '../../data/Plat';
import {MovieResponse} from '../../data/Movie';
import {PlatService} from '../../services/plat.service';
import {FilmsService} from '../../services/films.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {SearchService} from '../../services/search.service';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  cartDisplay = false;
  private user: User;
  private panier;
  private uid: string;
  private plats: Plat[] = [];
  private filmsList: MovieResponse[] = [];
  private searchQuery: '';
  myControl = new FormControl();
  filteredOptions: Observable<Object[]>;
  constructor(private searchService: SearchService,
              private router: Router,
              private afAuth: AngularFireAuth,
              private platService: PlatService,
              private panierService: PanierService,
              private filmService: FilmsService,
              private keywordResearchService: KeywordResearchService,
              private routeActivated: ActivatedRoute) {
    this.routeActivated.queryParams.subscribe((v) => {
      this.searchQuery = v.query;
    });

    this.afAuth.user.subscribe((user) => {
      if (user) {
        this.user = user;
        this.panierService.initPanier(user.uid);
        localStorage.setItem('userId', user.uid);
      } else {
        localStorage.removeItem('userId');
      }
    });

    searchService.setResetSearch(() => {
      this.searchQuery = '';
    });

    panierService.setRefreshPanierCallback(() => {
      this.getPanier();
    });

    this.uid =  localStorage.getItem('userId');
  }

  ngOnInit() {
    // this.searchQuery = this.router.getCurrentNavigation().extractedUrl.queryParams.query;
    this.myControl.valueChanges
      .subscribe({
        next: (value) => {
          if (!value) {
            return;
          }
          this.keywordResearchService.multiSearch(value).subscribe((r) => {
            this.filteredOptions = r.results;
          });
        }
      });
    this.getPanier();
  }

  getPanier() {
    // if (!this.isUserConnected()) {
    //   return;
    // }
    // Attente de la promesse pour récupérer le panier
    this.panierService.getPanier(this.uid).then(async value => {
      this.panier = value;
      if (!this.panier) {
        return;
      }
      // Attente de la promesse pour récupérer les différents plats
      const countsPlat = {};
      this.panier['plats'].forEach(function(x) {
        countsPlat[x] = (countsPlat[x] || 0) + 1;
      });
      this.panier['plats'] = [...new Set(this.panier['plats'])];

      const platsFetch = [];
      for (const idPlat of this.panier['plats']) {
        await this.platService.getPlat(+idPlat).then((plat) => {
          plat.quantite = countsPlat[idPlat];
          platsFetch.push(plat);
        });
      }
      const filmsFetch = [];
      for (const idFilm of this.panier['films']) {
        await this.filmService.getFilmByID(idFilm).subscribe((films) => {
          filmsFetch.push(films);
        });
      }
      this.plats = platsFetch;
      this.loadIMG();
      this.filmsList = filmsFetch;
    });
  }

  async loadIMG() {
    if (this.plats) {
      for (const p of this.plats) {
        let urlIMG;
        if (p.url === undefined) {
          urlIMG = environment.firebase.urlImgDefault;
        } else {
          urlIMG = p.url;
        }
        await this.platService.getImage(urlIMG).then((img) => {
          p.img = img;
        });
      }
    }
  }

  async loginGoogle() {
    await this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
    location.reload();
  }

  async deLoginGoogle() {
    await this.afAuth.auth.signOut();
    await this.afAuth.auth.app.delete();
    location.reload();
  }

  isUserConnected(): boolean {
    return !!this.user;
  }

  openPanier() {
    this.router.navigate(['/panier']);
  }

  displayFn(string) {
    if (string) {
      return string.name;
    }
  }

  showCart(bool) {
    this.cartDisplay = bool;
  }

  getTotal() {
    let montant = this.filmsList.length * 6;
    this.plats.forEach((p) => {
      montant += p.quantite ? p.prix * p.quantite : p.prix;
    });
    return montant;
  }

  linkToFilm(filmId) {
    this.router.navigate(['/films', filmId]);
  }

  handleSearch(event) {
    this.searchService.resetGenre();
    event.preventDefault();
    this.router.navigate(['/films'], { queryParams: {query: this.searchQuery}});
    this.searchService.search(this.searchQuery);
  }

  getQuantite() {
    let quantite = this.filmsList.length;
    this.plats.forEach(p => {
      quantite += p.quantite;
    });
    return quantite;
  }

  getFilmImg(poster) {
    if (poster === null) {
      return '/assets/img/default-placeholder.png';
    } else {
      return 'http://image.tmdb.org/t/p/original/' + poster;
    }
  }

  routerLinkFilm() {
    this.router.navigate(['/films'])
    this.searchService.search('');
  }
}
